/** COMP30024 Artificial Intelligence
 *  Authors:
 *  1. Johan Albert (albertj - 567375)
 *  2. Sande Harsa (sharsa - 567642)
 */

import java.util.Comparator;

/*
 * This class is used to compare the SLD distance from Node A to Node N and from Node B to Node N.
 */
public class sldComparator implements Comparator<Disk> {
	Disk n;

	public sldComparator(Disk node) {
		this.n = node;
	}

	@Override
	public int compare(Disk a, Disk b) {
		return a.sld(n) < b.sld(n) ? -1 : a.sld(n) == b.sld(n) ? 0 : 1;
	}
}