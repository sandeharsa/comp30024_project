/** COMP30024 Artificial Intelligence
 *  Authors:
 *  1. Johan Albert (albertj - 567375)
 *  2. Sande Harsa (sharsa - 567642)
 */

import java.util.ArrayList;


public class Cell {
	
	private int status;
	private Disk disk;
	
	/** Cell constructor 
	 * @param stat White (W), Black (B), or Empty (E)
	 */
	public Cell() {
		this.status = 0;
		this.disk = null;
	}

	/**
	 * Gets status of the cell (0 for empty, 1 for occupied)
	 * @return Status of the cell (0 or 1)
	 */
	public int getStatus() {
		return this.status;
	}
	
	/**
	 * Gets the disk that is in the cell
	 * @return The disk
	 */
	public Disk getDisk() {
		return this.disk;
	}
	
	/**
	 * Place a new disk in the cell
	 * @param x x position
	 * @param y y position
	 * @param type type of the disk (W/B = White/Black)
	 */
	public void placeDisk(int x, int y, char type) {
		this.disk = new Disk(x,y,type);
		this.status = 1;

		ArrayList<Cell> adjacentCells = this.getAdjacentCells(x, y);
		if(type == 'W')
			Board.diskWhite.add(disk);
		else
			Board.diskBlack.add(disk);
		
		for(int i = 0; i < adjacentCells.size(); i++) {
			if (adjacentCells.get(i).getStatus() == 1 &&
					adjacentCells.get(i).getDisk().getType() == this.getDisk().getType()) {
				this.getDisk().addNeighbor(adjacentCells.get(i).getDisk());
				adjacentCells.get(i).getDisk().addNeighbor(this.getDisk());
			}
		}
	}

	/**
	 * Gets adjacent cells of this cell
	 * @param x the x position of this cell
	 * @param y the y position of this cell
	 * @return
	 */
	public ArrayList<Cell> getAdjacentCells(int x, int y) {
		ArrayList<Cell> adjacentCells = new ArrayList<Cell>();
		Cell adjacent;
		
		//adjacent cells:
		//1. top-left: x-1, y-1
		if((adjacent = Board.getCell(x-1, y-1)) != null)
			adjacentCells.add(adjacent);
		//2. top-right: x-1, y
		if((adjacent = Board.getCell(x-1, y)) != null)
			adjacentCells.add(adjacent);
		//3. right: x, y+1
		if((adjacent = Board.getCell(x, y+1)) != null)
			adjacentCells.add(adjacent);
		//4. bot-right: x+1, y+1
		if((adjacent = Board.getCell(x+1, y+1)) != null)
			adjacentCells.add(adjacent);
		//5. bot-left: x+1, y
		if((adjacent = Board.getCell(x+1, y)) != null)
			adjacentCells.add(adjacent);
		//6. left: x, y-1
		if((adjacent = Board.getCell(x, y-1)) != null)
			adjacentCells.add(adjacent);
		
		return adjacentCells;
	}
}