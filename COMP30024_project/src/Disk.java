/** COMP30024 Artificial Intelligence
 *  Authors:
 *  1. Johan Albert (albertj - 567375)
 *  2. Sande Harsa (sharsa - 567642)
 */


import java.util.ArrayList;

public class Disk {
	
	private int x;
	private int y;
	private char type;
	private ArrayList<Disk> neighbor;
	private double fscore;
	
	public Disk(int x, int y, char type) {
		this.x = x;
		this.y = y;
		this.type = type;
		this.neighbor = new ArrayList<Disk>();
	}
	
	/**
	 * Gets X position of the disk
	 * @return x position of the disk
	 */
	public int getX() {
		return this.x;
	}
	
	/**
	 * Gets Y position of the disk
	 * @return y position of the disk
	 */
	public int getY() {
		return this.y;
	}
	/**
	 * Gets the type of the disk
	 * @return type of disk
	 */
	public char getType() {
		return this.type;
	}
	
	/**
	 * Gets the list of neighbors of this Disk
	 * @return neighbors
	 */
	public ArrayList<Disk> getNeighbor() {
		return neighbor;
	}
	
	/**
	 * Add a new neighbor
	 * @param newNeighbor the disk to be added as a neighbor
	 */
	public void addNeighbor(Disk newNeighbor) {
		neighbor.add(newNeighbor);
	}
	
	/**
	 * SLD = Straight Line Distance from one node (Disk) to another
	 * @param node The node (Disk) to calculate SLD from
	 * @return
	 */
	public double sld(Disk node) {
		int x,y,y1,y2;
		
		// x distance between node and goal
		x = node.getX() - this.getX();
		
		// if the node is in the lower part
		if (this.getX() >= Board.N) {
			y1 = this.getY() - Board.getLSP(node.getX());
		}
		else {
			y1 = node.getY();
		}
		// if the goal is in the lower part
		if (this.getX() >= Board.N) {
			y2 = this.getY() - Board.getLSP(this.getX());
		}
		else {
			y2 = this.getY();
		}
		// y distance between node and goal
		y = y1 - y2;
		
		// the sld distance
		return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
	}
	
	public double getFscore() {
		return this.fscore;
	}
	public void setFscore(double f) {
		this.fscore = f;
	}
}


